library shelf_route.router.builder.test;

import 'package:test/test.dart';
import 'package:shelf_route/src/route_impl.dart';
import 'package:shelf_route/src/router_builder.dart';
import 'package:shelf_route/src/default_adapters.dart';
import 'helpers.dart';

main() {
  group('build', () {
    testCase('when routes are empty',
        forBuilder: () => new DefaultRouterBuilder('top'),
        expectRouter: createRouter('top', []),
        expectBehaviour: [
          behaviour('any request',
              forRequest: () => createRequest('top'), expectNotHandled: true)
        ]);

    testCase('when router has simple routes',
        forBuilder: () => new DefaultRouterBuilder('top')
          ..get('foo', testHandler('one'))
          ..put('bar', testHandler('two')),
        expectRouter: createRouter('top', [
          simple('foo', ['GET'], true, 'one'),
          simple('bar', ['PUT'], true, 'two')
        ]),
        expectBehaviour: [
          behaviour('a request matching only top router',
              forRequest: () => createRequest('top'), expectNotHandled: true),
          behaviour('a request matching full route',
              forRequest: () => createRequest('top/foo'),
              expectResult: testResult(result: 'one')),
          behaviour('a request matching second route',
              forRequest: () => createRequest('top/bar', 'PUT'),
              expectResult: testResult(result: 'two'))
        ]);

    testCase('when router has heirarchical routes',
        forBuilder: () => (new DefaultRouterBuilder('top')
          ..get('foo', testHandler('one'))
          ..addAll(
              (DefaultRouterBuilder r) => r
                ..addAll((DefaultRouterBuilder r) =>
                        r..put('sub1sub1', testHandler('sub1sub1')),
                    path: '/subr1subr1'),
              path: '/subr1')),
        expectRouter: createRouter(
            'top',
            [
          simple('foo', ['GET'], true, 'one'),
          createRouter('subr1', [
            createRouter('subr1subr1', [
              simple('sub1sub1', ['PUT'], true, 'sub1sub1')
            ])
          ])
        ]),
        expectBehaviour: [
          behaviour('a request matching first route',
              forRequest: () => createRequest('top/foo'),
              expectResult: testResult(result: 'one')),
          behaviour('a request matching second route',
              forRequest: () =>
                  createRequest('top/subr1/subr1subr1/sub1sub1', 'PUT'),
              expectResult: testResult(result: 'sub1sub1'))
        ]);

    testCase('when router has heirarchical routes with a null path',
        forBuilder: () => (new DefaultRouterBuilder('top')
          ..get('foo', testHandler('one'))
          ..addAll(
              (DefaultRouterBuilder r) => r
                ..addAll((DefaultRouterBuilder r) =>
                        r..put('sub1sub1', testHandler('sub1sub1')),
                    path: '/subr1subr1'))),
        expectRouter: createRouter(
            'top',
            [
          simple('foo', ['GET'], true, 'one'),
          createRouter(null, [
            createRouter('subr1subr1', [
              simple('sub1sub1', ['PUT'], true, 'sub1sub1')
            ])
          ])
        ]),
        expectBehaviour: [
          behaviour('a request matching first route',
              forRequest: () => createRequest('top/foo'),
              expectResult: testResult(result: 'one')),
          behaviour('a request matching second route',
              forRequest: () => createRequest('top/subr1subr1/sub1sub1', 'PUT'),
              expectResult: testResult(result: 'sub1sub1'))
        ]);

    testCase('when router has middleware',
        forBuilder: () => (new DefaultRouterBuilder('top',
            routeAdapter: new DefaultRouterAdapter.def(),
            middleware: testMiddleware(1))
            ..get('foo', testHandler('one'), middleware: testMiddleware(-1))
            ..addAll(
                (DefaultRouterBuilder r) => r
                  ..addAll(
                      (DefaultRouterBuilder r) => r
                        ..put('sub1sub1', testHandler('sub1sub1'),
                            middleware: testMiddleware(-3)),
                      path: '/subr1subr1',
                      middleware: testMiddleware(3)),
                path: '/subr1',
                middleware: testMiddleware(2))),
        expectBehaviour: [
          behaviour('a request matching first route',
              forRequest: () => createRequest('top/foo'),
              expectResult: testResult(
                  result: 'one',
                  breadcrumbsIn: [1, -1],
                  breadcrumbsOut: [-1, 1])),
          behaviour('a request matching second route',
              forRequest: () =>
                  createRequest('top/subr1/subr1subr1/sub1sub1', 'PUT'),
              expectResult: testResult(
                  result: 'sub1sub1',
                  breadcrumbsIn: [1, 2, 3, -3],
                  breadcrumbsPathsIn: [
                    'subr1/subr1subr1/sub1sub1',
                    'subr1subr1/sub1sub1',
                    'sub1sub1',
                    ''
                  ],
                  breadcrumbsOut: [-3, 3, 2, 1]))
        ]);

    testCase('when router has handlerAdapters',
        forBuilder: () => (new DefaultRouterBuilder('top',
            routeAdapter: new DefaultRouterAdapter.def(
                handlerAdapter: testAdapter(plus5)),
            middleware: testMiddleware(1))
            ..get('foo', testFancyHandler('one'),
                middleware: testMiddleware(-1))
            ..addAll(
                (DefaultRouterBuilder r) => r
                  ..addAll(
                      (DefaultRouterBuilder r) => r
                        ..put('sub1sub1', testFancyHandler('sub1sub1'),
                            middleware: testMiddleware(-3),
                            handlerAdapter: testAdapter(times5))
                        ..delete('sub1sub1', testFancyHandler('sub1sub1'),
                            middleware: testMiddleware(-4),
                            handlerAdapter: fancyAdapter),
                      path: '/subr1subr1',
                      middleware: testMiddleware(3)),
                path: '/subr1',
                middleware: testMiddleware(2))),
        expectBehaviour: [
          behaviour('a request with a single adapter',
              forRequest: () => createRequest('top/foo'),
              expectResult: testResult(
                  result: 'one',
                  breadcrumbsIn: [1, -1].map(plus5),
                  breadcrumbsOut: [-1, 1])),
          behaviour('a request with merged handler adpaters',
              forRequest: () =>
                  createRequest('top/subr1/subr1subr1/sub1sub1', 'PUT'),
              expectResult: testResult(
                  result: 'sub1sub1',
                  breadcrumbsIn: [1, 2, 3, -3].map(plus5).map(times5),
                  breadcrumbsOut: [-3, 3, 2, 1])),
          behaviour('a request with non mergeable handler adpaters',
              forRequest: () =>
                  createRequest('top/subr1/subr1subr1/sub1sub1', 'DELETE'),
              expectResult: testResult(
                  result: 'sub1sub1',
                  breadcrumbsIn: [1, 2, 3, -4],
                  breadcrumbsOut: [-4, 3, 2, 1]))
        ]);

    testCase('when router has pathAdapters',
        forBuilder: () => (new DefaultRouterBuilder('top',
            routeAdapter: new DefaultRouterAdapter.def(
                pathAdapter: testPathAdapter('99')))
            ..get('foo', testHandler('one'))
            ..addAll(
                (DefaultRouterBuilder r) => r
                  ..addAll((DefaultRouterBuilder r) =>
                          r..put('sub1sub1', testHandler('sub1sub1')),
                      path: 'subr1subr1', pathAdapter: testPathAdapter(';;')),
                path: 'subr1')),
        expectRouter: createRouter(
            'top99',
            [
          simple('foo99', ['GET'], true, 'one'),
          createRouter('subr199', [
            createRouter('subr1subr1;;', [
              simple('sub1sub1;;', ['PUT'], true, 'sub1sub1')
            ])
          ])
        ]),
        expectBehaviour: [
          behaviour('a request matching first route',
              forRequest: () => createRequest('top99/foo99'),
              expectResult: testResult(result: 'one')),
          behaviour('a request matching second route',
              forRequest: () =>
                  createRequest('top99/subr199/subr1subr1;;/sub1sub1;;', 'PUT'),
              expectResult: testResult(result: 'sub1sub1'))
        ]);

    testCase('when router has routeableAdapters',
        forBuilder: () => (new DefaultRouterBuilder('top',
            routeAdapter: new DefaultRouterBuilderAdapter.def(
                routeableAdapter: new TestRouteableAdapter()))
            ..addAll(
                (TestRouteable r) => r
                  ..handlers['GET'] = testHandler('sub1sub1')
                  ..children['child1'] = (TestRouteable r) =>
                      r..handlers['PUT'] = testHandler('sub1sub1sub1'),
                path: 'subr1')),
        expectRouter: createRouter(
            'top',
            [
          createRouter('subr1', [
            simple('get', ['GET'], true, 'sub1sub1'),
            createRouter('child1', [
              simple('put', ['PUT'], true, 'sub1sub1')
            ])
          ])
        ]));
  });
}

testCase(String description,
    {DefaultRouterBuilderFactory forBuilder,
    RequestRouterImpl expectRouter,
    Iterable<ExpectedBehaviour> expectBehaviour}) {
  group(description, () {
    _testCase(description, forBuilder, expectRouter, expectBehaviour);
  });
}

_testCase(
    String description,
    DefaultRouterBuilderFactory forBuilder,
    RequestRouterImpl expectRouter,
    Iterable<ExpectedBehaviour> expectBehaviour) {
  final actualFactory = () => forBuilder().build();

  if (expectRouter != null) {
    routerCompare(actualFactory, expectRouter);
  }

  if (expectBehaviour != null) {
    group('then', () {
      for (var expected in expectBehaviour) {
        checkBehaviour(actualFactory, expected);
      }
    });
  }
}
