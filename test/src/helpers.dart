library helpers;

import 'package:shelf/shelf.dart';
import 'package:shelf_route/src/core.dart';
import 'package:uri/uri.dart';
import 'package:shelf_route/src/router_builder.dart';
import 'package:shelf_route/src/route_impl.dart';
import 'package:shelf_route/src/default_segment_router.dart';
import 'package:test/test.dart';
import 'package:shelf_route/src/route.dart';
import 'package:option/option.dart';
import 'package:shelf_route/src/default_route_impl.dart';

Handler testHandler(String result) => new TestHandler(result);
OohFancy testFancyHandler(String result) => new TestFancyHandler(result);

const String breadcrumbsOut = 'breadcrumbsOut';
const String breadcrumbsIn = 'breadcrumbsIn';

Request createRequest(String path, [String method = 'GET']) =>
    new Request(method, Uri.parse('http://example.com/$path'));

RequestRouterImpl createRouter(path, Iterable<Route> routes) =>
    new RequestRouterImpl(
        path, segmentRouter(path, null, false), routes, const None());

Middleware testMiddleware(int id) {
  return (Handler innerHandler) {
    return (Request r) async {
      final response = await innerHandler(
          r.change(context: addBreadCrumb(r.context, breadcrumbsIn, id, r)));
      return response.change(
          context: addBreadCrumb(response.context, breadcrumbsOut, id, r));
    };
  };
}

class BreadCrumb {
  final int value;
  final Request request;

  BreadCrumb(this.value, this.request);
}

Map addBreadCrumb(Map context, String key, int value, Request request) {
  final List<BreadCrumb> crumbs =
      new Map.from(context).putIfAbsent(key, () => []);
  crumbs.add(new BreadCrumb(value, request));
  return {key: crumbs};
}

Iterable<BreadCrumb> _getCrumbs(Map context, String key) => context[key];

// TODO: very dodgy. Originally only the int values were stored in the context
// and adapters etc built around it. Should clean up so it always contains
// an iterable of BreadCrumb
Iterable<int> getCrumbs(Map context, String key) =>
    _getCrumbs(context, key).map((bc) => bc is BreadCrumb ? bc.value : bc);

Iterable<String> getCrumbPaths(Map context, String key) =>
    _getCrumbs(context, key).map((bc) => bc.request.url.path);

class TestHandler {
  final String result;
  Request calledRequest;

  TestHandler(this.result);

  call(Request request) {
    calledRequest = request;
    return new Response.ok(result,
        context: {breadcrumbsIn: request.context[breadcrumbsIn]});
  }
}

typedef Response OohFancy(Iterable<int> breadcrumbsIn);

class TestFancyHandler {
  final String result;

  TestFancyHandler(this.result);

  Response call(Iterable<int> breadcrumbs) {
    return new Response.ok(result, context: {breadcrumbsIn: breadcrumbs});
  }
}

HandlerAdapter get fancyAdapter => doFancy;

Handler doFancy(OohFancy fancy) {
  return (Request request) => fancy(request.context[breadcrumbsIn]);
}

typedef int IntMap(int i);
int intMapIdentity(int i) => i;
int plus5(int i) => i + 5;
int times5(int i) => i * 5;

TestMergeableHandlerAdapter testAdapter(IntMap mapper) =>
    new TestMergeableHandlerAdapter(mapper);

class TestMergeableHandlerAdapter extends MergeableHandlerAdapter {
  final IntMap mapper;

  TestMergeableHandlerAdapter([this.mapper = intMapIdentity]);

  @override
  HandlerAdapter merge(TestMergeableHandlerAdapter other) =>
      new TestMergeableHandlerAdapter((int i) => mapper(other.mapper(i)));

  Handler call(OohFancy fancy) => doFancy(fancy);

  Handler doFancy(OohFancy fancy) {
    return (Request request) {
      final Iterable<int> bc = getCrumbs(request.context, breadcrumbsIn);
      final mapped = bc != null ? bc.map(mapper) : [];
      return fancy(mapped);
    };
  }
}

PathAdapter testPathAdapter(String suffix) =>
    (path) => new UriParser(new UriTemplate('$path$suffix'));

typedef TestRouteableFunction(TestRouteable router);

class TestRouteableAdapter {
  RouteableFunction call(Function handler) => adapt(handler);

  RouteableFunction adapt(TestRouteableFunction handler) {
    return (DefaultRouterBuilder builder) {
      final routeable = new TestRouteable();
      handler(routeable);

      routeable.handlers.forEach((method, h) {
        builder.add(method.toLowerCase(), [method], h);
      });

      routeable.children.forEach((path, child) {
        builder.addAll(child, path: path);
      });
    };
  }
}

class TestRouteable {
  final Map<String, Handler> handlers = {};
  final Map<String, TestRouteableFunction> children = {};

  TestRouteable();
}

SimpleRouteImpl simple(String path, Iterable<String> methods, bool exactMatch,
        String handlerResult) =>
    new SimpleRouteImpl(path, segmentRouter(path, methods, exactMatch),
        new TestHandler(handlerResult));

typedef DefaultSegmentRouter DefaultSegmentRouterFactory();
typedef DefaultRouterBuilder DefaultRouterBuilderFactory();
typedef Request RequestFactory();
typedef DefaultRouterImpl RouterFactory();
typedef SimpleRouteImpl SimpleRouteFactory();

DefaultSegmentRouter segmentRouter(
        String path, Iterable<String> methods, bool exactMatch) =>
    new DefaultSegmentRouter(
        path != null ? new UriParser(new UriTemplate(path)) : null,
        methods,
        exactMatch);

TestRouteResult testResult(
        {String result,
        Iterable<String> breadcrumbsIn,
        Iterable<String> breadcrumbsPathsIn,
        Iterable<String> breadcrumbsOut}) =>
    new TestRouteResult(
        result, breadcrumbsIn, breadcrumbsPathsIn, breadcrumbsOut);

class TestRouteResult {
  final String result;
  final Iterable<String> breadcrumbsIn;
  final Iterable<String> breadcrumbsPathsIn;
  final Iterable<String> breadcrumbsOut;
  TestRouteResult(this.result, this.breadcrumbsIn, this.breadcrumbsPathsIn,
      this.breadcrumbsOut);
}

ExpectedBehaviour behaviour(String description,
        {RequestFactory forRequest,
        TestRouteResult expectResult,
        bool expectNotHandled: false}) =>
    new ExpectedBehaviour(
        description, forRequest, expectResult, expectNotHandled);

class ExpectedBehaviour {
  final String description;
  final RequestFactory forRequest;
  final TestRouteResult expectResult;
  final bool expectNotHandled;

  ExpectedBehaviour(this.description, this.forRequest, this.expectResult,
      this.expectNotHandled);
}

void checkBehaviour(
    RouterFactory actualFactory, ExpectedBehaviour expectBehaviour) {
  group(expectBehaviour.description, () {
    RequestRouterImpl actual;

    setUp(() {
      actual = actualFactory();
    });

    if (expectBehaviour.expectNotHandled) {
      test('is not handled', () {
        expect(actual.handle(expectBehaviour.forRequest()),
            throwsA(new isInstanceOf<NotHandled>()));
      });
    } else {
      test('is handled and contains response', () async {
        final actualResult = await actual.handle(expectBehaviour.forRequest());
        expect(actualResult, new isInstanceOf<Response>());
      });

      group('is handled with', () {
        final TestRouteResult expectedResult = expectBehaviour.expectResult;
        Response response;

        setUp(() async {
          response = await actual.handle(expectBehaviour.forRequest());
        });

        test('expected result', () async {
          expect(await response.readAsString(), equals(expectedResult.result));
        });

        if (expectedResult.breadcrumbsIn != null) {
          test('expected request middleware breadcrumbs', () {
            expect(getCrumbs(response.context, breadcrumbsIn),
                orderedEquals(expectedResult.breadcrumbsIn));
          });
        }

        if (expectedResult.breadcrumbsPathsIn != null) {
          test('expected request middleware breadcrumb request paths', () {
            expect(getCrumbPaths(response.context, breadcrumbsIn),
                orderedEquals(expectedResult.breadcrumbsPathsIn));
          });
        }

        if (expectedResult.breadcrumbsOut != null) {
          test('expected response middleware breadcrumbs', () {
            expect(getCrumbs(response.context, breadcrumbsOut),
                orderedEquals(expectedResult.breadcrumbsOut));
          });
        }
      });
    }
  });
}

void routerCompare(
    final RouterFactory actualFactory, final RequestRouterImpl expected) {
  RequestRouterImpl actual;
  setUp(() {
    actual = actualFactory();
  });

  test('has expected number of routes', () {
    expect(actual.routes, hasLength(expected.routes.length));
  });

  group('has segment router with', () {
    segmentRouterCompare(() => actual.segmentRouter, expected.segmentRouter);
  });

  for (int i = 0; i < expected.routes.length; i++) {
    group('then route $i', () {
      var expectedRoute = expected.routes.elementAt(i);
      var actualRouteFactory = () => actual.routes.elementAt(i);

      if (expectedRoute is SimpleRouteImpl) {
        test('is a SimpleRoute', () {
          expect(actualRouteFactory(), new isInstanceOf<SimpleRouteImpl>());
        });
        simpleRouteCompare(() => actual.routes.elementAt(i), expectedRoute);
      } else if (expectedRoute is RequestRouterImpl) {
        test('is a Router', () {
          expect(actualRouteFactory(), new isInstanceOf<RequestRouterImpl>());
        });
        routerCompare(actualRouteFactory, expectedRoute);
      } else {
        fail('unexpected type of route $expectedRoute');
      }
    });
  }
}

void simpleRouteCompare(
    final SimpleRouteFactory actualFactory, final SimpleRouteImpl expected) {
  SimpleRouteImpl actual;
  setUp(() {
    actual = actualFactory();
  });

  group('has a segment router with', () {
    segmentRouterCompare(() => actual.segmentRouter, expected.segmentRouter);
  });
}

void segmentRouterCompare(final DefaultSegmentRouterFactory actualFactory,
    final DefaultSegmentRouter expected) {
  DefaultSegmentRouter actual;

  setUp(() {
    actual = actualFactory();
  });

  test('expected path', () {
    expect(actual.path.toString(), equals(expected.path.toString()));
  });

  test('expected methods', () {
    if (expected.methods != null) {
      expect(actual.methods, unorderedEquals(expected.methods));
    } else {
      expect(actual.methods, isNull);
    }
  });
}
