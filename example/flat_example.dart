// Copyright (c) 2015, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart' as io;
import 'package:shelf_route/shelf_route.dart' as r;
import 'package:shelf_exception_handler/shelf_exception_handler.dart';

void main() {
  var router = r.router()
    ..get('/', (_) => new Response.ok("Hello World"))
    ..post('/', (_) => new Response.ok("Hello World"))
    ..put('/', (_) => new Response.ok("Hello World"))
    ..delete('/', (_) => new Response.ok("Hello World"),
        middleware: logRequests())
    ..add('/multimethod', ['GET', 'PUT'], (_) => new Response.ok("Hello World"))
    ..get(
        '/greeting/{name}',
        (request) =>
            new Response.ok("Hello ${r.getPathParameter(request, 'name')}"))
    ..get(
        '/greeting2/{name}{?age}',
        (request) {
      var name = r.getPathParameter(request, 'name');
      var age = r.getPathParameter(request, 'age');
//      SimpleRouteImpl route = r.getRoutingContext(request).get().currentRoute;
      return new Response.ok("Hello $name of age $age");
    });

  var handler = const Pipeline()
      .addMiddleware(logRequests())
      .addMiddleware(exceptionHandler())
      .addHandler(router.handler);

  r.printRoutes(router);

  io.serve(handler, 'localhost', 8080).then((server) {
    print('Serving at http://${server.address.host}:${server.port}');
  });
}
