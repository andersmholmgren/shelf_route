// Copyright (c) 2015, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_route.router.segment.standard;

import 'package:shelf/shelf.dart';
import 'package:uri/uri.dart';
import 'package:path/path.dart' as p;
import 'package:shelf_path/shelf_path.dart';
import 'core.dart';
import 'route.dart';
import 'routing_context.dart';
import 'dart:async';
import 'segment_router.dart';

final _url = p.url;

/// The default implementation of [SegmentRouter] that routes based on the
/// [Request] [path] and http [methods]
class DefaultSegmentRouter extends SegmentRouter {
  final UriPattern path;
  final Iterable<String> methods;
  final bool exactMatch;

  DefaultSegmentRouter(this.path, this.methods, this.exactMatch);

  Future<Response> handle(Request request, Handler handler, Route route) async {
    final adjustedRequest = _createNewRequest(request, route);
    return await handler(adjustedRequest);
  }

  bool canHandle(Request request) {
    final method = request.method;
    final url = request.url;

    if (methods != null && !methods.contains(method)) {
      return false;
    }

    if (path == null) {
      return true;
    }

    final patternMatches = path.matches(url);

    if (!patternMatches) {
      return false;
    }

    if (!exactMatch) {
      return true;
    }

    final uriMatch = path.match(url);
    final remainingSegments = uriMatch.rest.pathSegments;

    return remainingSegments.length == 0 ||
        (remainingSegments.length == 1 &&
            (remainingSegments.first == '' || remainingSegments.first == '/'));
  }

  Request _createNewRequest(Request request, Route route) {
    final url = request.url;

    final newContext = {}..addAll(request.context);
    appendToRoutingBreadCrumbs(newContext, route);

    if (path == null) {
      return request.change(context: newContext);
    }

    final uriMatch = path.match(url);

    /*
     * Make sure we use the same method to split in all cases.
     * We don't use url.pathSegments as it URI decodes the segments and
     * we don't want that when we reconstruct the newPath later.
     * Instead we manually decode when passing path parameters
     * into request context
     */
    Iterable<String> _pathSegments(String path) => _url.split(path);

    final requestSegments = _pathSegments(url.path);

    final skipCount = request.handlerPath.isNotEmpty &&
        requestSegments.isNotEmpty &&
        requestSegments.first == '/' ? 1 : 0;
    final remainingPath = _cleanRest(uriMatch.rest);

    final extraScriptPath = requestSegments.skip(skipCount).take(
        requestSegments.length - _pathSegments(remainingPath.path).length);

    final newPath = Uri.parse(_url.joinAll(extraScriptPath)).toString();

    if (uriMatch.parameters.isNotEmpty) {
      // must decode here as we don't use url.pathSegments
      final parameters = <String, Object>{};
      uriMatch.parameters.forEach((k, v) {
        parameters[k] = (v != null)? Uri.decodeComponent(v) : null;
      });
      addPathParametersToContext(newContext, parameters);
    }

    return request.change(context: newContext, path: newPath);
  }

  // workaround for https://github.com/google/uri.dart/issues/16
  Uri _cleanRest(Uri rest) {
    if (rest.hasAuthority || rest.fragment == '' || rest.query == '') {
      final builder = new UriBuilder.fromUri(rest);
      if (builder.fragment == '') {
        builder.fragment = null;
      }
      if (builder.host == '') {
        builder.host = null;
      }
      if (builder.queryParameters != null && builder.queryParameters.isEmpty) {
        builder.queryParameters = null;
      }

      return builder.build();
    }
    return rest;
  }

  @override
  DefaultSegmentRouter join(SegmentRouter child) {
    if (child is! DefaultSegmentRouter) {
      throw new StateError('doh need to rethink this');
    } else {
      final DefaultSegmentRouter childSegmentRouter = child;

      final newPath = new UriParser(new UriTemplate(
          _url.join(path.toString(), childSegmentRouter.path.toString())));
      return new DefaultSegmentRouter(
          newPath,
          _httpMethodIntersection(methods, childSegmentRouter.methods),
          childSegmentRouter.exactMatch);
    }
  }

  PathInfo get pathInfo =>
      new PathInfo(_methodsToString(methods), '/${path.toString()}');

  String toString() => '$methods $path';
}

Iterable<String> _httpMethodIntersection(
    Iterable<String> l1, Iterable<String> l2) {
  if (l1 == null && l2 == null) {
    return null;
  }

  Set<String> _set(Iterable<String> l) => l != null ? l.toSet() : allMethods;

  return _set(l1).intersection(_set(l2));
}

final Set<String> allMethods =
    ['GET', 'POST', 'PUT', 'DELETE', 'OPTION'].toSet();

String _methodsToString(Iterable<String> methods) {
  return methods == null ? '*' : methods.join(',');
}
