// Copyright (c) 2015, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_route.router.builder;

import 'route.dart' show RouterBuilder;
import 'package:shelf/shelf.dart';
import 'package:uri/uri.dart';
import 'package:path/path.dart' as p;
import 'router_builder_impl.dart';
import 'core.dart';
import 'package:option/option.dart';
import 'default_route.dart';

final _url = p.url;

/// An abstract specification of a [Route]. A [RouteSpec] requires a matching
/// [RouteAdapter] to turn it into a [Route].
///
/// This is a key extension mechanism in shelf_route. It allows other packages
/// to provide a combination of specs and adapters such that [Route]s can be
/// produced from any sensible external form such as classes and their methods
/// and properties, using annotations etc.
abstract class RouteSpec<R extends DefaultRoute> {
  /// A name for a route
  String get name;

  /// Some representation of a route's path
  get path;

  /// an accompanying adapter for the spec.
  RouteAdapter<R> get routeAdapter;

  Middleware get middleware;
}

/// Provides the data that will be used to create a [SimpleRoute].
/// The [routeAdapter] if provided will be combined with existing adapters in
/// the router and the result used to adapt the spec into a [SimpleRoute].
/// This includes adapting the [handler] into a [Handler], the [path] into a
/// [UriPattern] plus including any provided [Middleware]
abstract class SimpleRouteSpec extends RouteSpec<DefaultSimpleRoute> {
  Function get handler;
  bool get exactMatch;
  Iterable<String> get methods;

  factory SimpleRouteSpec(
      String name,
      path,
      Iterable<String> methods,
      bool exactMatch,
      Function handler,
      SimpleRouteAdapter routeAdapter,
      Middleware middleware) = DefaultSimpleRouteSpec;
}

/// Provides the data that will be used to create a [RequestRouter].
/// The [routeAdapter] if provided will be combined with existing adapters in
/// the router and the result used to adapt the definition into a [Router].
/// [routes] must only contain objects of type [RouterDefinition] and
/// [SimpleRouteSpec]
abstract class RouterSpec extends RouteSpec<DefaultRouter> {
  Iterable<RouteSpec> get routes;
}

/// An adapter that converts a [RouteSpec] into a [Route].
/// The combination of pairs of [RouteSpec]s and their [RouteAdapter]s provides
/// a lot of flexibility in the creation of [Route]s
abstract class RouteAdapter<R extends DefaultRoute> {
  /// Adapt the [RouteSpec] into a [Route]
  R adapt(RouteSpec<R> routeSpec);

  /// Merge this [RouteAdapter] with the owning [RouterAdapter]. This allows
  /// RouteAdapters to inherit properties from their parents.
  RouteAdapter<R> merge(RouterAdapter routerAdapter);
}

/// An adapter that adapts an external form of a [SimpleRoute] into an object
/// of type [SimpleRoute]. This allows other libraries to extend shelf_route
abstract class SimpleRouteAdapter extends RouteAdapter<DefaultSimpleRoute> {}

/// An adapter that adapts an external form of a [Router] into an object
/// of type [Router]. This allows other libraries to extend shelf_route
abstract class RouterAdapter extends RouteAdapter<DefaultRouter> {}

/// a RouterBuilder that uses pairs of [RouteSpec] and [RouteAdapter] to define
/// routes in an abstract, extensible way
abstract class SpecBasedRouterBuilder<B extends DefaultRouterBuilder>
    extends RouterBuilder<DefaultRouter> implements RouterSpec {
  factory SpecBasedRouterBuilder(path,
      {String name,
      RouterAdapter routeAdapter,
      routeable}) = SpecBasedRouterBuilderImpl;

  void addRoute(RouteSpec spec);

  /// adds a [SimpleRoute] based on the provided [spec].
  /// This method is mainly intended for other frameworks to integrate with
  /// shelf_route. The other methods are normally more convenient to use
  void addSimpleRoute(SimpleRouteSpec spec);

  /// adds a [Router] based on the provided [spec].
  /// This method is mainly intended for other frameworks to integrate with
  /// shelf_route. The other methods are normally more convenient to use
  void addRouter(RouterSpec spec);
}

/// A [SpecBasedRouterBuilder] that defines a simple API for creating routes
abstract class DefaultRouterBuilder<B extends DefaultRouterBuilder>
    extends SpecBasedRouterBuilder<B> implements RouteableRouterSpec {
  Option get routeable;

  factory DefaultRouterBuilder(path,
      {String name,
      RouterAdapter routeAdapter,
      routeable,
      Middleware middleware}) = DefaultRouterBuilderImpl;

  /**
   * Adds a route with the given [handler], [path] and [method].
   *
   * [path] may be either a String or a [UriPattern]. If path is a String it
   * will be parsed as a [UriParser] which means it is expected to conform to
   * a [UriTemplate].
   *
   * The [handler] must be of type [Handler] unless a custom [handlerAdapter]
   * has been set for this router. The [handlerAdapter] will be applied to
   * the provided [handler] to create the actual handler used.
   *
   * If [exactMatch] is true then a path such as `/foo/bar/fum` will not match
   * a path like `/foo/bar`, where as when it is false it will.
   *
   * If [middleware] is included a pipeline will be created with the middleware
   * and [handler]
   *
   * If [handlerAdapter] is provided it will be merged with the parent adapter
   * if it exists and both are of type [MergeableHandlerAdapter]. This allows
   * custom tweaking of creating the handler per route.
   */
  void add(dynamic path, List<String> methods, Function handler,
      {bool exactMatch: true,
      String name,
      Middleware middleware,
      HandlerAdapter handlerAdapter});

  /// adds all the routes created by [routeable]'s [createRoutes] method
  /// to the current router if [path] is not provided or to a new child [Router]
  /// at [path].
  ///
  /// [routeable] must conform to the [RouteableFunction] typedef unless
  /// a [RouteableAdapter] is either passed to this method or already installed
  /// in this [Router].
  ///
  /// If [middleware] is included a pipeline will be created with the middleware
  /// and [handler]
  void addAll(routeable,
      {dynamic path,
      String name,
      Middleware middleware,
      HandlerAdapter handlerAdapter,
      RouteableAdapter routeableAdapter,
      PathAdapter pathAdapter});

  /// adds a child [Router] at [path]
  B child(dynamic path,
      {String name,
      RouteableAdapter routeableAdapter,
      PathAdapter pathAdapter,
      HandlerAdapter handlerAdapter,
      Middleware middleware});

  /// creates a route with method = GET
  void get(dynamic path, Function handler,
      {String name, Middleware middleware, HandlerAdapter handlerAdapter});

  /// creates a route with method = POST
  void post(dynamic path, Function handler,
      {String name, Middleware middleware, HandlerAdapter handlerAdapter});

  /// creates a route with method = PUT
  void put(dynamic path, Function handler,
      {String name, Middleware middleware, HandlerAdapter handlerAdapter});

  /// creates a route with method = DELETE
  void delete(dynamic path, Function handler,
      {String name, Middleware middleware, HandlerAdapter handlerAdapter});
}
