// Copyright (c) 2015, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_route.router.standard;

import 'package:shelf/shelf.dart';
import 'route.dart';
import 'default_segment_router.dart';
import 'package:uri/uri.dart';

/// The default implementation of [Route]. This is the most common kind of
/// Route and is the basis of the core builders.
/// Routing is done using the associated [DefaultSegmentRouter] which routes
/// on the [Request] path and method
abstract class DefaultRoute implements Route {
  DefaultSegmentRouter get segmentRouter;
  Handler get handler;
}

/// The default implementation of [RequestRouter].
abstract class DefaultRouter extends DefaultRoute
    implements RequestRouter<DefaultRoute> {}

/// The default implementation of [SimpleRoute].
abstract class DefaultSimpleRoute extends DefaultRoute implements SimpleRoute {}

abstract class DefaultRoutePath implements RoutePath<DefaultRoute> {
  UriPattern get fullPath;
}
