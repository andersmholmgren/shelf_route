// Copyright (c) 2015, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_route.router.fundamentals;

import 'package:shelf/shelf.dart';
import 'core.dart';
import 'dart:async';
import 'package:option/option.dart';

/// The root of the Router hierarchy. A Route is something capable of routing
/// a [Request].
///
/// Note: this is a very abstract implementation. In almost all cases it is more
/// convenient to use subclasses like [DefaultRoute]
abstract class Route {
//  String get id;

  /// Routes can have a name
  String get name;

  /// return true if this Route can handled the [request]
  bool canHandle(Request request);

  /// Route the request and return a future [Response].
  /// This must only be called if [canHandle] return true
  Future<Response> handle(Request request);

  /// Visitor pattern support
  void accept(RouteVisitor visitor);

  /// flattens the [Route] into a list of [SimpleRoute]s. This turns the tree
  /// structure of the [Route] hierarchy into a single List
  Iterable<SimpleRoute> get flattened;

  /// Creates a [Handler] from this Route given a [fallbackHandler] which will
  /// be called if no routes match
  Handler createHandler(Handler fallbackHandler);

  Option<RoutePath> findByName(String name);
}

/// A [Route] that routes by attempting to route to a list of child [Route]s
/// in order. The first [Route] that handles the request wins.
///
/// This represents a tree node within the hierarchy
///
/// Note: this is a very abstract implementation. In almost all cases it is more
/// convenient to use subclasses like [DefaultRouter]
abstract class RequestRouter<R extends Route> extends Route {
  /// The child [Route]s
  Iterable<R> get routes;

  Iterable<PathInfo> get flattenedPaths;
}

/// A leaf node in the hierarchy. A SimpleRoute will route by checking if
/// the [Request] matches the conditions for the route and if so handling
/// the request directly
///
/// Note: this is a very abstract implementation. In almost all cases it is more
/// convenient to use subclasses like [DefaultSimpleRoute]
///
abstract class SimpleRoute extends Route {}

/// Thrown by a [RequestRouter] if no routes match the request
class NotHandled implements Exception {
  const NotHandled();
}

/// The base of all builders for [RequestRouter]s
abstract class RouterBuilder<R extends RequestRouter> {
  /// Builds and returns the created subtype of [RequestRouter]
  R build();
}

/// Visitor pattern support
abstract class RouteVisitor {
  visitSimple(SimpleRoute simple);
  visitRouter(RequestRouter router);
}

abstract class RoutePath<R extends Route> {
  Iterable<RequestRouter<R>> get parents;

  R get route;
}
