// Copyright (c) 2015, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_route.router;

import 'package:shelf/shelf.dart';

import 'api_impl.dart' as impl;
import 'router_builder.dart';
import 'core.dart';
import 'dart:math';
import 'default_adapters.dart';

/**
 * A [Handler] that routes to other handlers based on the incoming request
 * (path and method). Note there can be several levels of routers to facilitate
 * modularity.
 *
 * When the Router routes a request to a handler it adjusts the requests as follows:
 *
 * * the routes path is removed from the start of the requests url
 * * the routes path is added to the end of the requests handlerPath
 *
 *
 * For example if original request had url of `banking` and handlerPath was
 * `/abc` then when routing a request of `/banking/accounts` the new request
 * passed to the handler will have a url of `accounts` and handlerPath of
 * `/abc/banking`
 *
 * If [fallbackHandler] is provided it will be called if no routes match. If
 * omitted a default handler which returns a 404 response is used
 *
 * If [middleware] is included a pipeline will be created with the middleware
 * and [handler]
 *
 * A [HandlerAdapter] can be provided to allow handlers that do not conform
 * to the [Handler] typedef to be used. For example,
 * [shelf_bind](https://pub.dartlang.org/packages/shelf_bind) provides such
 * an adapter.
 *
 * A [RouteableAdapter] can be provided to allow functions that do not conform
 * to the [RouteableFunction] typedef to be used. For example,
 * [shelf_rest](https://pub.dartlang.org/packages/shelf_rest) provides such
 * an adapter.
 *
 */

Router router(
        {HandlerAdapter handlerAdapter: noopHandlerAdapter,
        RouteableAdapter routeableAdapter: noopRouteableAdapter,
        PathAdapter pathAdapter: uriTemplatePattern,
        Function fallbackHandler: impl.send404,
        Middleware middleware}) =>
    new impl.ShelfRouteRouterBuilder.create(
        handlerAdapter: handlerAdapter,
        routeableAdapter: routeableAdapter,
        pathAdapter: pathAdapter,
        fallbackHandler: fallbackHandler,
        middleware: middleware);

/// Used to specify that the Router should route requests to all HTTP methods
/// for a given route
const ALL_METHODS = null;

typedef void Printer(String s);

/// prints out a representation of all the routes within the router, using
/// the [printer] if provided or the standard Dart [print] function if not.
///
void printRoutes(Router router, {Printer printer: print}) {
  final paths = router.fullPaths;
  final longest = paths.fold(0, (t, pi) => max(t, pi.method.length));

  Iterable<String> _padding(int length) {
    return new List.filled(longest - length, ' ');
  }

  String _padded(String s) {
    return s + _padding(s.length).join('');
  }
  paths.forEach((pi) {
    printer('${_padded(pi.method)}\t->\t${pi.path}');
  });
}

/// Builds [Middleware] for routing requests
abstract class Router<B extends Router> implements DefaultRouterBuilder<B> {
  Iterable<PathInfo> get fullPaths;

  /// Returns a shelf [Handler] for the router
  Handler get handler;
}

/// A class capable of creating routes in a given [Router].
/// In most cases `RouteableFunction` is simpler and more compact
abstract class Routeable<R extends Router> {
  void createRoutes(R router);
  void call(R router) => createRoutes(router);
}
