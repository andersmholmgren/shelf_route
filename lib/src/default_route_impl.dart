// Copyright (c) 2015, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_route.router.standard.impl;

import 'package:shelf/shelf.dart';
import 'default_route.dart';
import 'route_impl.dart';
import 'package:option/option.dart';
import 'default_segment_router.dart';
import 'package:shelf_route/src/route.dart';
import 'package:uri/uri.dart';
import 'package:quiver/iterables.dart';

class DefaultRouterImpl
    extends RequestRouterImpl<DefaultSegmentRouter, DefaultRoute>
    implements DefaultRouter {
  DefaultRouterImpl(String name, DefaultSegmentRouter segmentRouter,
      Iterable<DefaultRoute> routes, Option<Middleware> middleware)
      : super(name, segmentRouter, routes, middleware);

  RoutePath createRoutePath(
          Iterable<RequestRouter<DefaultRoute>> parents, DefaultRoute route) =>
      new DefaultRoutePathImpl(parents, route);
}

class DefaultSimpleRouteImpl extends SimpleRouteImpl<DefaultSegmentRouter>
    implements DefaultSimpleRoute {
  DefaultSimpleRouteImpl(
      String name, DefaultSegmentRouter segmentRouter, Handler handler)
      : super(name, segmentRouter, handler);
}

class DefaultRoutePathImpl extends RoutePathImpl<DefaultRoute>
    implements DefaultRoutePath {
  UriPattern get fullPath => new UriParser(new UriTemplate(
      concat([parents, [route]])
          .map((DefaultRoute r) => r.segmentRouter.path.toString())
          .where((segment) => segment != null && segment.isNotEmpty)
          .join('/')));

  DefaultRoutePathImpl(
      Iterable<RequestRouter<DefaultRoute>> parents, DefaultRoute route)
      : super(parents, route);
}
